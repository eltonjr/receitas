# Molho pesto de manjericão


## Ingredientes

- 4 dentes de alho
- 1 colher (chá) de sal
- 1 xícara (chá) de folhas de manjericão, fresco
- 3 colheres (chá) de pinoli ou nozes sem casca (ou Castanhas do Pará trituradas)
- 100 g de queijo pecorino ou parmesão ralado
- 1/2 xícara (chá) de azeite
- pimenta-do-reino a gosto

## Modo de preparo

Descasque os dentes de alho, passe pelo espremedor e coloque em uma tigela.

Adicione a colher de chá de sal e misture muito bem.

Triture os pinoli (ou as nozes ou as castanhas) (no processador, com um pilão ou com as mãos) e junte alho e sal.

Lave as folhas de manjericão, seque e pique em pedaços bem pequenos, depois coloque também na tigela.

Acrescente o queijo e o azeite e misture muito bem até obter uma pasta homogênea.

Tempere com um pouco de pimenta-do-reino e conserve em um vidro esterilizado.

## Referencia

https://www.tudogostoso.com.br/receita/5887-molho-pesto.html