# Sobrecoxa de frango na airfryer

## Ingredientes

1. sobrecoxas com osso, +- 1kg, 5 peças
1. sal grosso temperado (famoso a gosto, mas é mais ou menos 1 colher de sopa)
1. azeite, +- 1 colher de sopa

## Modo de preparo

1. pré aqueça a air fryer a 185 graus, por 3 a 5 minutos
1. em um recipeinte, colocar azeite sobre as sobrecoxas, salpicar o sal grosso.
1. misturar o azeite e o sal nas sobrecoxas; deixar descansar de 3 a 5 minutos.
1. colocar as sobrecoxas no cesto da airfryer, com a pele pra cima, em uma única camada, evitando deixar uma por cima das outras
1. assar por aprox. 40 a 185 graus (esse foi o tempo pré-definido na minha)
1. quando faltar uns 10 minutos, virar a pele pra baixo para dourar também embaixo.

## Tempo de preparo

entre 45 a 50 minutos