# Molho Barbecue

## Ingredientes

1. 1 xícara de açúcar mascavo
1. 1/2 xícara de molho inglês
1. 1 xícara de molho de tomate ao sugo, qualquer molho pronto serve
1. 2 colheres (sopa) de mostarda
1. 2 dentes de alho amassados
1. 1/2 colherinha (chá) de sal

## Modo de preparo

Coloque todos os ingredientes em uma frigideira em fogo médio e mexa até apurar.

O molho vai engrossar.

## Referência

https://www.tudogostoso.com.br/receita/15757-molho-barbecue.html